import java.util.Iterator;

/**
 * EvaderRanger
 * 
 * Quazi Hameem Mahmud
 * Iain Watson Smith
 * Elena Williams
 * 
 * Semester 2, 2011 Assignment
 * Final Due: 19-Oct-2011
 */

public class Game {
	Integer xdim, ydim; 
	boolean array [][];
	static int scale;

	static final Integer numberHench = 4;
	static final Double henchGapx = 250.0;
	static final Double henchGapy = 50.0;
	
	static final Double playerStartx = EvaderRanger.xcanvas/2.0 - Player.width/2.0;
	static final Double playerStarty = EvaderRanger.ycanvas/2.0 - Player.height;

	static final Double banditStartx = (double) Bandit.width*5.0;
	static final Double banditStarty = (double) EvaderRanger.ycanvas - Bandit.height*5.0;
	
	Double xsize, ysize;
	Boolean won;
	
	Player player;
	Bandit bandit;
	Accomplices henchmen;
	
public Game(Double xd, Double yd) {
		won = false;
		
		xdim = (int)Math.round(xd);
		ydim = (int)Math.round(yd);

		scale = 15;
		array =  new boolean[xdim/scale][ydim/scale];
		
		player = new Player(array, scale, playerStartx, playerStarty);
		bandit = new Bandit(array, scale, banditStartx, banditStarty);
		henchmen = new Accomplices();

		for (Integer i = 1; i < numberHench; i++) {
			henchmen.add(new Accomplice(array, scale, henchGapx*i, henchGapy*i));
		}
	}
	
	public void draw(GameComponent canvas) {
		for (Accomplice b : henchmen) {
			b.draw(this, canvas);
		}	
		player.draw(this, canvas);
		bandit.draw(this, canvas);
	}

	boolean xtrue;
    boolean ytrue;
    boolean henchxtrue;
    boolean henchytrue;
    
    public void step(GameComponent canvas) {
        Double xGoal = player.x;
        Double yGoal = player.y;    

        for (Accomplice b : henchmen) {
            
            Double henchx = b.x;
            Double henchy = b.y;
            
            if(henchx < xGoal && b.xVel < 0 && henchxtrue == true){
                b.xVel = -b.xVel;
                henchxtrue = false;
            }
            else if(henchy < yGoal && b.yVel < 0 && henchytrue == true){
                b.yVel = -b.yVel;
                henchytrue = false;
            }
            else if(henchx > xGoal && b.xVel > 0 && henchxtrue == false){
                b.xVel = -b.xVel;
                henchxtrue = true;
            }
            else if(henchy > yGoal && b.yVel > 0 && henchytrue == false){
                b.yVel = -b.yVel;
                henchytrue = true;
            }
            
            b.step(this, canvas);
            if(player.collide(b)){
                if(player.xVel>0){
                    player.xVel=1.0;
                }
                if(player.yVel>0){
                    player.yVel=1.0;
                }
                if(player.xVel<0){
                    player.xVel=-1.0;
                }
                if(player.yVel<0){
                    player.yVel=-1.0;
                }
                b.xVel=player.xVel;
                b.yVel=player.yVel;
                nextHench++;
            }
        }

        if(bandit.x > xGoal && bandit.xVel < 0 && xtrue==true){
            bandit.xVel = -bandit.xVel;
            xtrue = false;
        }
        else if(bandit.y > yGoal && bandit.yVel < 0&&ytrue == true){
            bandit.yVel = -bandit.yVel;
            ytrue = false;
        }
        else if(bandit.x < xGoal && bandit.xVel > 0 && xtrue == false){
            bandit.xVel = -bandit.xVel;
            xtrue = true;
        }
        else if(bandit.y < yGoal && bandit.yVel > 0 && ytrue == false){
            bandit.yVel = -bandit.yVel;
            ytrue = true;
        }
        
        player.step(this, canvas);
        bandit.step(this, canvas);

        if (player.collide(bandit)) {
            won = true;
            System.out.println("You have won!");
        }
    }
}
