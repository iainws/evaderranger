import java.util.Random;

/**
 * EvaderRanger
 * 
 * Generate Map
 * main author: Iain Watson Smith
 * Quazi Hameem Mahmud
 * Elena Williams
 * 
 * Semester 2, 2011 Assignment
 * Final Due: 19-Oct-2011
 */

// build the collision array

public class GenerateMap {
    static int xDim;
    static int yDim;
    static boolean [][] array;
    static int depth;
    static int density;
    static int d;
    static int scale;
    GenerateMap(int xDim,int yDim, boolean [][] array,int depth,int density,int scale){
        this.xDim = xDim;
        this.yDim = yDim;
        this.array = array;
        this.depth = depth;
        this.density = density;
        this.scale = scale;
    }
    
    //initialize the array  
    
    void init(){
        for(int i=0;i<xDim;++i)for(int j=0;j<yDim;++j)array[i][j]=false;
        for(int i=0;i<xDim;++i){array[i][0]=true;array[i][yDim-1]=true;}
        for(int j=0;j<yDim;++j){array[0][j]=true;array[xDim-1][j]=true;}
    }

    void createMap(){
        init();
        Random rand = new Random();
        for(int i=0;i<density;++i){
            int x = rand.nextInt(xDim - 1);
            int y = rand.nextInt(yDim - 1);
            d=depth;
            createBlock(x,y);
        }
    }

    //add the blocks 

    void createBlock(int i, int j){
        d = d - 1;
        if(d == 0)return;
        
        if(i + 1 < xDim)array[i+1][j]=true;
        if(j + 1 < yDim)array[i][j+1]=true;
        if(i - 1 >= 0)array[i-1][j]=true;
        if(j - 1 >= 0)array[i][j-1]=true;
        
        if(i + 1 < xDim && j + 1 < yDim & i - 1 >=0 && j - 1 >= 0){
        	
            Random rand = new Random();
            
            int x = rand.nextInt(4);
            
            if(x == 0){
                createBlock(i+1,j);
            }
            if(x == 1){
                createBlock(i-1,j);
            }
            if(x == 2){
                createBlock(i,j+1);
            }
            if(x == 3){
                createBlock(i,j-1);
            }
        }
        else return;
    }
}
