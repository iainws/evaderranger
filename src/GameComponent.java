import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 * EvaderRanger
 * 
 * Quazi Hameem Mahmud
 * Iain Watson Smith
 * Elena Williams
 * 
 * Semester 2, 2011 Assignment
 * Final Due: 19-Oct-2011
 */

/* Added in the method to help players and bandits check their positions next to the walls 06/10/2011 */

@SuppressWarnings("serial")
public class GameComponent extends JComponent implements KeyListener {
	Integer xdim, ydim; 
	private BufferedImage background;
	private BufferedImage offscreen;

	Boolean lPress, rPress, uPress, dPress;
	Boolean qPress;
	boolean array [][];
	static int scale;
	
	//make background
	
	public GameComponent(int xd, int yd) {
		xdim = xd;
		ydim = yd;
		this.setSize(xdim, ydim);
		this.setPreferredSize(new Dimension(xdim,ydim));

		scale = 15;
		array =  new boolean[xdim/scale][ydim/scale];
		GenerateMap Board = new GenerateMap(xdim/scale,ydim/scale,array,20,20,scale);
		Board.createMap();	
		BoardImage b = new BoardImage(xdim,ydim,array,scale);
        try{
    		background = ImageIO.read(new File("board.png"));
        }
        catch(IOException e){
            e.printStackTrace();
        }
		offscreen = new BufferedImage(xdim, ydim, BufferedImage.TYPE_INT_RGB);
		addKeyListener(this);
		this.setFocusable(true);
		lPress = false;
		rPress = false;
		uPress = false;
		dPress = false;
		qPress = false;
	}
	
	public void clearOffscreen() {
		Graphics g = offscreen.getGraphics();
		g.drawImage(background, 0, 0, null);
	}

	public Graphics getBackgroundGraphics() {
		return background.getGraphics();
	}

	public Graphics getOffscreenGraphics() {
		return offscreen.getGraphics();
	}

	public void drawImage(Image i, int x, int y) {
		Graphics g = offscreen.getGraphics();
		g.drawImage(i, x, y, null);
	}

	public void drawOffscreen() {
		Graphics g;
		g = this.getGraphics();
		g.drawImage(offscreen, 0, 0, null);
	}

	public void paint(Graphics g) {
		g.drawImage(offscreen, 0, 0, null);
	}

	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_KP_LEFT
				|| e.getKeyCode() == KeyEvent.VK_LEFT) {
			lPress = true;
		} else if (e.getKeyCode() == KeyEvent.VK_KP_RIGHT
				|| e.getKeyCode() == KeyEvent.VK_RIGHT) {
			rPress = true;
		} else if (e.getKeyCode() == KeyEvent.VK_KP_UP
				|| e.getKeyCode() == KeyEvent.VK_UP) {
			uPress = true;
		} else if (e.getKeyCode() == KeyEvent.VK_KP_DOWN
				|| e.getKeyCode() == KeyEvent.VK_DOWN) {
			dPress = true;
		} else if (e.getKeyCode() == KeyEvent.VK_ESCAPE
				|| e.getKeyCode() == KeyEvent.VK_Q) {
			qPress = true;
		}
	}

	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_KP_LEFT
				|| e.getKeyCode() == KeyEvent.VK_LEFT) {
			lPress = false;
		} else if (e.getKeyCode() == KeyEvent.VK_KP_RIGHT
				|| e.getKeyCode() == KeyEvent.VK_RIGHT) {
			rPress = false;
		} else if (e.getKeyCode() == KeyEvent.VK_KP_UP
				|| e.getKeyCode() == KeyEvent.VK_UP) {
			uPress = false;
		} else if (e.getKeyCode() == KeyEvent.VK_KP_DOWN
				|| e.getKeyCode() == KeyEvent.VK_DOWN) {
			dPress = false;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {	
	}
}
