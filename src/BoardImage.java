import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * EvaderRanger
 * 
 * Generate Map
 * main author: Iain Watson Smith
 * Quazi Hameem Mahmud
 * Elena Williams
 * 
 * Semester 2, 2011 Assignment
 * Final Due: 19-Oct-2011
 */

public class BoardImage {

	static final Color borderColor = Color.decode("0x201b1c"); //Color.RED; 1b1a1f
	static final Color [] colors = new Color[]{
            Color.decode("0x362628"), 
            Color.decode("0x262a31"), 
            Color.decode("0x293036"), 
            Color.decode("0x1b1920"), 
            Color.decode("0x141b26"), 
            Color.decode("0x232025"), 
            Color.decode("0x261d28"), 
            Color.decode("0x322529"), 
            Color.decode("0x402c30"), 
            Color.decode("0x3d3034"), 
            Color.decode("0x232025"), 
            Color.decode("0x141b26"), 
            Color.decode("0x293036"), 
            Color.decode("0x202b37"), 
            Color.decode("0x202732"), 
            Color.decode("0x362628"),
	};
    static int colorBlock;

    BoardImage(int xdim,int ydim, boolean [][] array, int scale){
        Random rand = new Random();
        BufferedImage board = new BufferedImage(xdim,ydim,BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = board.createGraphics();       
        g2d.setColor(colors[0]);

        Image img = null;
        try {
		    img = ImageIO.read(new File("bg.jpg"));
        }
        catch(IOException e){
            e.printStackTrace();
        }

        int backcolor = 2;
        for(int i = 0; i < xdim/scale; ++i)for(int j = 0; j < ydim/scale; ++j){
            g2d.setColor(colors[backcolor]);
            g2d.fillRect(i*scale,j*scale,scale,scale);
            g2d.drawImage(img, 0, 0, null);
        }

        //paint the blocks on the map

        for(int i = 0; i < xdim/scale; ++i)for(int j = 0; j < ydim/scale; ++j){
            if(array[i][j]==true){
                colorBlock++;
                g2d.setColor(borderColor);
                g2d.fillRect(i*scale,j*scale,scale,scale);
                g2d.setColor(colors[rand.nextInt(colors.length)]);
                g2d.fillRect(i*scale+1,j*scale+1,scale-2,scale-2);
            }
        }

        //write the image

        try{
            ImageIO.write(board,"png",new File("board.png"));
        }
        catch(IOException e){
            e.printStackTrace();

        }
    }
}
