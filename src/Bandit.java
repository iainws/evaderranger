import javax.swing.ImageIcon;

/**
 * EvaderRanger
 * 
 * Quazi Hameem Mahmud
 * Iain Watson Smith
 * Elena Williams
 * 
 * Semester 2, 2011 Assignment
 * Final Due: 19-Oct-2011
 */

public class Bandit extends Agent {
	static final ImageIcon bandit = new ImageIcon(EvaderRanger.class.getResource("bandit.jpg"));
	static final Integer width = bandit.getIconWidth();
	static final Integer height = bandit.getIconHeight();
	static Double xVel = 2.5;
	static Double yVel = 2.5;
	boolean [][] array;
	int scale;

	//public Bandit(Double xp, Double yp) {
	public Bandit(boolean [][] array, int scale, Double xp, Double yp) {
		super();
		this.x = xp;
		this.y = yp;
		this.array = array;
		this.scale = scale;
	}

	public void draw(Game h, GameComponent canvas) {
		canvas.drawImage(bandit.getImage(), mapx(h, canvas, x-(width/3)),
			mapy(h, canvas, y-(height/3)));
	}
	
	public Double width() {
		return (double) width;
	}

	public Double height() {
		return (double) height;
	}

	public void step(Game bw, GameComponent canvas) {
		//Double [] velocities = canvas.updateXVel(xVel, yVel,x,y);
		collideGame(xVel,yVel);
		x += xVel;
		y += yVel;
	}
}
