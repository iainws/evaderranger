import java.util.Random;
import javax.swing.ImageIcon;

/**
 * EvaderRanger
 * 
 * main author: Elena Williams
 * Quazi Hameem Mahmud
 * Iain Watson Smith
 * 
 * Semester 2, 2011 Assignment
 * Final Due: 19-Oct-2011
 */

public class Accomplice extends Agent {

	static Integer width;
	static Integer height;
	
	Double speed = 2.0;
	Double xVel = 2.0;
	Double yVel = 2.0;

	boolean [][] array;
	int scale;
	
	public Accomplice(boolean [][] array, int scale, double xp, double yp) {
		super();
		this.x = xp;
		this.y = yp;
		this.array = array;
		this.scale = scale;
	}

	public void draw(Game h, GameComponent canvas) {
	    Random rand = new Random();
		String henchGraphic = "hench" + rand.nextInt(3) + ".jpg";
		ImageIcon hench = new ImageIcon(EvaderRanger.class.getResource(henchGraphic));
		width = hench.getIconWidth();
		height = hench.getIconHeight();
		
		canvas.drawImage(hench.getImage(), mapx(h, canvas, x), mapy(h, canvas, y));
	}
	
	public void step(Game bw, GameComponent canvas) {
		//Double [] velocities = collideGame(xVel, yVel,x,y);
		
		//xVel = velocities[0];
		//yVel = velocities[1];		
		
		collideGame(xVel,yVel);
		
		x += xVel;
		y += yVel;
	}
	
	public Double width() {
		return (double) width;
	}

	public Double height() {
		return (double) height;
	}
}
